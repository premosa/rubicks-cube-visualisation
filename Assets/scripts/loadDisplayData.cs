﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class loadDisplayData : MonoBehaviour {

    // filename and data string variables
    private string filename = "RB_cube.txt";
    private string data = "";

    // Prepare materials (colours for tiles)
    public Material red;
    public Material blue;
    public Material green;
    public Material orange;
    public Material white;
    public Material yellow;

    // This function is called with button click
    public void doVisualization() {
        prepareVariable();
        readFile();
        visualize();
    }

    private void prepareVariable()
    {
        data = "";
        position = 0;
    }

    //IMPORTANT: txt file must be in root directory of Unity project ( /Assets/Resources/{{filename}} ) ---> {{filename}} ... Angular notation for place variable filename value here
    private void readFile() {

        string path = "Assets/Resources/" + filename;
        
        //Read the text from directly from the test.txt file
        StreamReader reader = new StreamReader(path);
        string result = reader.ReadToEnd();
        reader.Close();

        data = result;

        Debug.Log("Read input is: " + result);
    }

    private int position;
    private string[] faces = { "face1", "face2", "face3", "face4", "face5", "face6" };
    private string[] squares = { "sq1", "sq2", "sq3", "sq4", "sq5", "sq6", "sq7", "sq8", "sq9" };
    private void visualize()
    {
        GameObject currentFace;
        GameObject currentSquare;
        Material currentMaterial;
        int i,j;
        for (i = 0; i < 6; i++) {
            currentFace = gameObject.transform.Find(faces[i]).gameObject; //from current gameobject, locate proper face
            for (j = 0; j < 9; j++) {
                currentSquare = currentFace.transform.Find(squares[j]).gameObject;
                currentMaterial = findMaterial(ref position);
                if (currentMaterial == null)
                {
                    Debug.Log("We have read unknown material state... Ending function");
                    return;
                }
                else {
                    currentSquare.GetComponent<Renderer>().material = currentMaterial;
                }
            }
            position++;
        }
        //
    }

    private Material findMaterial(ref int position) {

        Debug.Log("position: " + position);
        Debug.Log("Data: " + data);

        char c = data[position];
        position++;

        if (c.Equals('O')) { return orange; }
        else if (c.Equals('B')) { return blue; }
        else if (c.Equals('G')) { return green; }
        else if (c.Equals('R')) { return red; }
        else if (c.Equals('Y')) { return yellow; }
        else if (c.Equals('W')) { return white; }
        else
        {
            Debug.Log("Unknown character: " + c);
            return null;
        }
    }
}
