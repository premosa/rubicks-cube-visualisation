import math
import cv2
import numpy as np

###############################
#  PROGRAMME USAGE
###############################
'''
KEYS EXPLAINED

SPACE - Turn or on off whether we want to allow face capturing or not(disable it to center cube face into white circle, then enable)

Y - when we captured a face save it as good result (analyse the top-left block)
N - when we capture a face clear this result and try capturing the same face again
( Y na N both reset allow_capture to 0 )

Q - quit application (does not save and status in progress)

--------------------------------------------------------------------------------------
UI EXPLAINED

In top-left we have colours recognised from cube state
In bottom-left we have cube unpacked that shows us which face we need to capture
^ First face doesnt matter in what orientation we capture, but all next ones must be logical 3D 90-degree rotations based on that INITIAL face (don't do unnecesarry rotation on Z-axis)
'''


###############################
#  FUNCTIONS
###############################

#returns cosine angle from pt0 -> pt1 and pt0-> pt2
def angle(pt1,pt2,pt0):
    #remove one parent structure that is indtroducted
    pt0 = pt0[0]
    pt1 = pt1[0]
    pt2 = pt2[0]

    dx1 = pt1[0] - pt0[0]
    dy1 = pt1[1] - pt0[1]
    dx2 = pt2[0] - pt0[0]
    dy2 = pt2[1] - pt0[1]
    return (dx1 * dx2 + dy1 * dy2) / math.sqrt((dx1 * dx1 + dy1 * dy1) * (dx2 * dx2 + dy2 * dy2) + 1e-10);

def findSquares(image,squares=[]):
    #return sequence of squares on the image
    #sequnce is stored in memory storage
    gray0 = cv2.cvtColor(image,cv2.COLOR_BGR2GRAY)
    gray0 = cv2.medianBlur(gray0, 5)
    thres = cv2.adaptiveThreshold(gray0, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY, 11, 2)
    thres = cv2.erode(thres,kernel=np.ones((3,3),np.uint8),iterations=2)
    gray = cv2.Canny(thres,0,10,3)
    im2, contours, hierarchy = cv2.findContours(gray, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

    for contour in contours:
        approx = cv2.approxPolyDP(contour,9,True)

        if len(approx) == 4 and abs(cv2.contourArea(approx)) > 7 and cv2.isContourConvex(approx):
            maxCosine = 0

            for j in range(2,5): # this loops whole values [2,5]
                cosine = abs(angle(approx[j%4],approx[j-2],approx[j-1]))
                maxCosine = max(maxCosine,cosine)

            if maxCosine < 0.3:
                squares.append(approx)

def check_if_center_inside_face(center,squares):
    result = 0
    for sq in squares:
        state = cv2.pointPolygonTest(contour=sq,pt=center,measureDist=False)
        if state > 0:
            # print("Point inside contour!")
            return sq
    return None

def returnImageColour(image,pos_x,pos_y):
    return image[pos_y,pos_x]

def colourDistance(colour1,colour2):
    (r1, g1, b1) = colour1
    (r2, g2, b2) = colour2
    return math.sqrt((r1 - r2) ** 2 + (g1 - g2) ** 2 + (b1 - b2) ** 2)

def performColourRecognition(colours):

    sample_blue = (169, 131, 97)
    sample_red = (136, 130, 194)
    sample_orange = (132,137,243)
    sample_green = (132, 178, 87)
    sample_white = (223, 230, 229)
    sample_yellow = (146, 221, 213)

    sampleColours = []
    sampleColours.append(sample_blue)
    sampleColours.append(sample_red)
    sampleColours.append(sample_orange)
    sampleColours.append(sample_green)
    sampleColours.append(sample_white)
    sampleColours.append(sample_yellow)

    #calcuate distance to each sample_colour; that with minimal distance is mostly likely the colour
    results = []
    labels = []
    for colour in colours:

        # BLUE colour - set initial state
        label = "B"
        cindex = 0
        minDistance = colourDistance(colour,sample_blue)

        #iterate sample colours
        #RED colour
        newDistance = colourDistance(colour,sample_red)
        if minDistance > newDistance:
            label = "R"
            cindex = 1
            minDistance = newDistance

        # ORANGE colour
        newDistance = colourDistance(colour, sample_orange)
        if minDistance > newDistance:
            label = "O"
            cindex = 2
            minDistance = newDistance

        # GREEN colour
        newDistance = colourDistance(colour, sample_green)
        if minDistance > newDistance:
            label = "G"
            cindex = 3
            minDistance = newDistance

        # WHITE colour
        newDistance = colourDistance(colour, sample_white)
        if minDistance > newDistance:
            label = "W"
            cindex = 4
            minDistance = newDistance

        # YELLOW colour
        newDistance = colourDistance(colour, sample_yellow)
        if minDistance > newDistance:
            label = "Y"
            cindex = 5
            minDistance = newDistance

        #append result
        results.append((sampleColours[cindex]))
        labels.append(label)
    return (results,labels)

def findColours(image,middle_contour):
    (x,y,w,h) = cv2.boundingRect(middle_contour)
    center_x = int(x + w/2)
    center_y = int(y + h/2)

    colours = []
    #find other 8 colours based on this contour size (because cube should have coloured panels of same size)
    panel_bot_left = image[int(center_y + 1.5 * h),int(center_x - 1.5 * w)]
    panel_bot_mid = image[int(center_y + 1.5 * h), center_x]
    panel_bot_right = image[int(center_y + 1.5 * h), int(center_x + 1.5 * w)]

    panel_left = image[center_y, int(center_x - 1.5 * w)]
    panel_mid = image[center_y,center_x]
    panel_right = image[center_y, int(center_x + 1.5 * w)]

    panel_top_left = image[int(center_y - 1.5 * h), int(center_x - 1.5 * w)]
    panel_top_mid = image[int(center_y - 1.5 * h), center_x]
    panel_top_right = image[int(center_y - 1.5 * h), int(center_x + 1.5 * w)]

    colours.append((int(panel_top_left[0]), int(panel_top_left[1]), int(panel_top_left[2])))
    colours.append((int(panel_top_mid[0]), int(panel_top_mid[1]), int(panel_top_mid[2])))
    colours.append((int(panel_top_right[0]), int(panel_top_right[1]), int(panel_top_right[2])))

    colours.append((int(panel_left[0]), int(panel_left[1]), int(panel_left[2])))
    colours.append((int(panel_mid[0]), int(panel_mid[1]), int(panel_mid[2])))
    colours.append((int(panel_right[0]), int(panel_right[1]), int(panel_right[2])))

    colours.append((int(panel_bot_left[0]), int(panel_bot_left[1]), int(panel_bot_left[2])))
    colours.append((int(panel_bot_mid[0]), int(panel_bot_mid[1]), int(panel_bot_mid[2])))
    colours.append((int(panel_bot_right[0]), int(panel_bot_right[1]), int(panel_bot_right[2])))

    # At this point, we can also do the following step
    colours = performColourRecognition(colours) #bellow is explained why this is not the ideal method
    # alot better method is MACHINE LEARNING: Find a large dataset of what colour sets a face can be (learning samples)
    # Our new (non-input) samples can then be colours we parse
    # Simply hard coding colour is a) really weak to area lightning b) very dependant on cube sample

    return colours

def add_to_output(output,labels):
    for label in labels:
        output = output + label
    output = output + " "
    return output

def write_to_file(output):
    f = open('RB_cube.txt', 'w')
    f.write(output)
    f.close()
    print("Result written to file...")

def drawSquares(image,squares):
    for c in squares: #c stands for contour; that is what is inside squares array
        (x, y, w, h) = cv2.boundingRect(c)
        cv2.rectangle(image, (x, y), (x + w, y + h), (0, 255, 0), 2)

def drawCurrentFace(window_size,image, currentFace):
    colours = []
    for count in range(0,6):
        colours.append((255,255,255))
    colours[currentFace] = (0,0,255)
    #draw all 6 faces of cube unpacked
    #deppending on currentFace we highlight face we must capture next
    window_width = window_size[0]
    window_height = window_size[1]
    # at top-left, create 3x3 grid - set parameters here
    grid_size = int(min(window_width,
                        window_height) * 0.08)  # grid size is some percent of smaller frame measurement (this is size of ONE 3x3 block)
    grid_spacing = 3

    #first 3 vertical
    cv2.rectangle(img=frame,
                  pt1=(grid_spacing,window_height-(grid_spacing*3+grid_size*3)),
                  pt2=(grid_spacing+grid_size,window_height-(grid_spacing*3+grid_size*2)),
                  color=colours[0], thickness=-1)
    cv2.rectangle(img=frame,
                  pt1=(grid_spacing, window_height - (grid_spacing * 2 + grid_size * 2)),
                  pt2=(grid_spacing + grid_size, window_height - (grid_spacing * 2 + grid_size * 1)),
                  color=colours[1], thickness=-1)
    cv2.rectangle(img=frame,
                  pt1=(grid_spacing, window_height - (grid_spacing + grid_size )),
                  pt2=(grid_spacing + grid_size, window_height - (grid_spacing)),
                  color=colours[2], thickness=-1)

    #add 3 horizontally to second face
    cv2.rectangle(img=frame,
                  pt1=(grid_spacing * 2 + grid_size, window_height - (grid_spacing * 2 + grid_size * 2)),
                  pt2=(grid_spacing * 2 + grid_size * 2, window_height - (grid_spacing * 2 + grid_size * 1)),
                  color=colours[3], thickness=-1)
    cv2.rectangle(img=frame,
                  pt1=(grid_spacing * 3 + grid_size *2, window_height - (grid_spacing * 2 + grid_size * 2)),
                  pt2=(grid_spacing * 3 + grid_size * 3, window_height - (grid_spacing * 2 + grid_size * 1)),
                  color=colours[4], thickness=-1)
    cv2.rectangle(img=frame,
                  pt1=(grid_spacing * 4 + grid_size * 3, window_height - (grid_spacing * 2 + grid_size * 2)),
                  pt2=(grid_spacing * 4 + grid_size * 4, window_height - (grid_spacing * 2 + grid_size * 1)),
                  color=colours[5], thickness=-1)


def drawCubeState(window_size,image,colours):
    window_width = window_size[0]
    window_height = window_size[1]
    # at top-left, create 3x3 grid - set parameters here
    grid_size = int(min(window_width,
                        window_height) * 0.06)  # grid size is some percent of smaller frame measurement (this is size of ONE 3x3 block)
    grid_spacing = 3
    # Drawing top row
    cv2.rectangle(img=frame, pt1=(0, 0), pt2=(grid_size, grid_size), color=face_colours[0], thickness=-1)  # top-left
    cv2.rectangle(img=frame, pt1=(grid_size + grid_spacing, 0), pt2=(grid_size * 2 + grid_spacing, grid_size),
                  color=face_colours[1], thickness=-1)  # top-mid
    cv2.rectangle(img=frame, pt1=(grid_size * 2 + grid_spacing * 2, 0),
                  pt2=(grid_size * 3 + grid_spacing * 2, grid_size), color=face_colours[2], thickness=-1)  # top-right
    # Drawing mid row
    cv2.rectangle(img=frame, pt1=(0, grid_size + grid_spacing), pt2=(grid_size, grid_size * 2 + grid_spacing),
                  color=face_colours[3], thickness=-1)
    cv2.rectangle(img=frame, pt1=(grid_size + grid_spacing, grid_size + grid_spacing),
                  pt2=(grid_size * 2 + grid_spacing, grid_size * 2 + grid_spacing), color=face_colours[4], thickness=-1)
    cv2.rectangle(img=frame, pt1=(grid_size * 2 + grid_spacing * 2, grid_size + grid_spacing),
                  pt2=(grid_size * 3 + grid_spacing * 2, grid_size * 2 + grid_spacing), color=face_colours[5],
                  thickness=-1)
    # Drawing bot row
    cv2.rectangle(img=frame, pt1=(0, grid_size * 2 + grid_spacing * 2),
                  pt2=(grid_size, grid_size * 3 + grid_spacing * 2), color=face_colours[6], thickness=-1)
    cv2.rectangle(img=frame, pt1=(grid_size + grid_spacing, grid_size * 2 + grid_spacing * 2),
                  pt2=(grid_size * 2 + grid_spacing, grid_size * 3 + grid_spacing * 2), color=face_colours[7],
                  thickness=-1)
    cv2.rectangle(img=frame, pt1=(grid_size * 2 + grid_spacing * 2, grid_size * 2 + grid_spacing * 2),
                  pt2=(grid_size * 3 + grid_spacing * 2, grid_size * 3 + grid_spacing * 2), color=face_colours[8],
                  thickness=-1)


###############################
#  MAIN
###############################

cap = cv2.VideoCapture(0) #set capture device
window_width = int(cap.get(cv2.CAP_PROP_FRAME_WIDTH))
window_height = int(cap.get(cv2.CAP_PROP_FRAME_HEIGHT))

print("window width: " + str(window_width))
print("window height: " + str(window_height))

#prepare font variable for text
font = cv2.FONT_HERSHEY_SIMPLEX

#define center height and width
center_x = int(window_height/2)
center_y = int(window_width/2)

#waitkey variables
waitkey_length = 10

#state 0 - capture new face
#state 1 - check if user wants to save this or throw away results
state = 0

# this variable determines when we want to allow or disallow face capture
allow_capture = 0

#output is string, consisting of 54 characters (for each face)
output = ""

# [0,5] integer value that defines which face to capture (defines progress of capturing)
currentFace = 0

while True: #iterate until we exit application

    # Capture frame-by-frame
    ret, frame = cap.read()

    squares = []

    if state == 0:
        # prepare default colours
        face_colours = []
        for i in range(0, 9):
            face_colours.append((255, 255, 255))


        findSquares(frame, squares)
        if allow_capture == 1:
            middle_contour = check_if_center_inside_face((center_y, center_x), squares)
            if middle_contour is not None:
                (face_colours,labels) = findColours(frame,
                                           middle_contour)  # Point is inside a contour and contour is atleast of some minimum size
                state = 1  # we found some combination, change state
            else:
                cv2.putText(frame, "Move middle square to circle",
                            org=(int(window_width * 0.2), int(window_height * 0.1)), fontFace=font, fontScale=1,
                            color=(255, 255, 255), thickness=2)

    #in bottom left show which face we want to capture
    drawCurrentFace(window_size=(window_width,window_height),image=frame,currentFace=currentFace)

    #in top left, draw state of colours
    drawCubeState(window_size=(window_width,window_height),image=frame,colours=face_colours)

    #draw mid circle, squares on image & draw the image itself
    if state == 0:
        cv2.circle(frame, (center_y, center_x), 10, face_colours[4], thickness=2, lineType=16, shift=0)
        drawSquares(frame, squares)
    else:
        cv2.putText(frame, "Do you wish to keep this face?", org=(int(window_width * 0.2), int(window_height * 0.1)),fontFace=font, fontScale=1, color=(255, 255, 255), thickness=2)
        cv2.putText(frame, "Yes(Y) or No(N)?", org=(int(window_width * 0.2), int(window_height * 0.16)),fontFace=font, fontScale=1, color=(255, 255, 255), thickness=2)



    #BUTTON PRESSING UI PART
    pressed_key = cv2.waitKey(waitkey_length)
    if pressed_key == ord('q'):
        #quit
        break
    elif pressed_key == ord(' '):
        #allow data capture
        allow_capture = (allow_capture+1)%2
        print("allow capture:" + str(allow_capture))
    elif pressed_key == ord('y'):
        print("Labels: " + str(labels))
        if currentFace == 5: #TODO CHANGE THIS TO 5
            output = add_to_output(output=output,labels=labels)
            write_to_file(output)
            #reset
            output = ""
            currentFace = 0
        else:
            output = add_to_output(output=output, labels=labels)
            currentFace = currentFace+1
        labels = []
        state = 0
        allow_capture = 0
    elif pressed_key == ord('n'):
        labels = []
        state = 0
        allow_capture = 0



    #DISPLAY FRAME
    cv2.imshow('frame', frame)

# When everything done, release the capture
cap.release()
cv2.destroyAllWindows()

print("Programme has finished...")